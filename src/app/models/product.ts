export class Product {
  id: number;
  description: string;
  price: number;
  imageUrl: string;
  title: string;

  constructor(id, title, description = '', price = 0, imageUrl = '') {
    this.id = id
    this.title = title
    this.description = description
    this.price = price
    this.imageUrl = imageUrl
  }
}
